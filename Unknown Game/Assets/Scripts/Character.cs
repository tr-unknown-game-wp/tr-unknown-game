﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : GameObject
{
	
	public float walkSpeed;
	
	new private Rigidbody rigidbody;
	private Vector3 lastInputVector;
	private bool receivedCollisionEvents;
	private bool colliding;
	public bool onFloor;
	private List<ContactPoint> contactsHelper;
	
    override public void Start()
    {
		base.Start();
        rigidbody = GetComponent<Rigidbody>();
		contactsHelper = new List<ContactPoint>(100);
    }

    override public void Update()
    {
		base.Update();
    }
	
	void FixedUpdate()
	{
		if(receivedCollisionEvents && !colliding)
			onFloor = false;
		receivedCollisionEvents = false;
		colliding = false;
		float horizontalInput = Input.GetAxisRaw("Horizontal");
		float verticalInput = Input.GetAxisRaw("Vertical");
		float jumpInput = Input.GetAxisRaw("Jump");
		bool walkable = onFloor && rigidbody.velocity.y < 0.01;
		
		Vector2 inputVector = (Vector2.up * verticalInput + Vector2.right * horizontalInput).normalized;
		if(inputVector != Vector2.zero){
			lastInputVector = new Vector3(inputVector.x, 0, inputVector.y);
			Vector2 currentVelocity = new Vector2(rigidbody.velocity.x, rigidbody.velocity.z);
			float directedVelocity = Vector2.Dot(currentVelocity, inputVector);
			if(directedVelocity < walkSpeed){
				float velocityChange = Mathf.Min(walkSpeed - directedVelocity, (walkable ? 250 : 10) * Time.deltaTime);
				currentVelocity += inputVector * velocityChange;
				rigidbody.velocity = new Vector3(currentVelocity.x, rigidbody.velocity.y, currentVelocity.y);
			}
		}
		if(lastInputVector != Vector3.zero){
			Quaternion targetRotation = Quaternion.LookRotation(lastInputVector);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 600 * Time.deltaTime);
		}
		if(walkable && jumpInput > 0){
			rigidbody.velocity = new Vector3(rigidbody.velocity.x, 10, rigidbody.velocity.z);
			onFloor = false;
		}
		if(onFloor)
			rigidbody.velocity *= 0.8f;
		else
			rigidbody.velocity *= 0.99f;
	}
	
	void OnCollisionEnter(Collision collision){
		receivedCollisionEvents = true;
		updateOnFloor(collision);
	}
	
	void OnCollisionExit(){
		receivedCollisionEvents = true;
	}
	
	void OnCollisionStay(Collision collision){
		receivedCollisionEvents = true;
		updateOnFloor(collision);
	}
	
	private void updateOnFloor(Collision collision){
		if(!colliding)
			onFloor = false;
		colliding = true; 
		contactsHelper.Clear();
		collision.GetContacts(contactsHelper);
		foreach(ContactPoint contact in contactsHelper){
			if(contact.normal.y > 0){
				onFloor = true;
				break;
			}
		}
	}
	
}
