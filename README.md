# Unknown Game by Tunt and Rut

# Info
Creation date: 22.06.20  
Current Unity version in use: 2019.4.1f1

### Setup repo on a new machine
1. Install currently used version of Unity (look at Info section of this README.md).
1. IMPORTANT: before cloning repo, install git lfs if not pre-installed with git.
1. Clone the repository, add the project folder to Unity Hub, run the project in Unity.
1. Verify Unity settings:
	* In Unity go to Edit > Project Settings > Editor.
		* Version Control / Mode: “Visible Meta Files”;
		* Asset Serialization / Mode: “Force Text”.
